# ADController Modules API
[![Generic badge](https://img.shields.io/badge/Release-v1.0-<COLOR>.svg)](https://shields.io/)<br>
[![Generic badge](https://img.shields.io/badge/Release-v1.1-<COLOR>.svg)](https://shields.io/)
  - fix a bug when changing the damper position auto offset.
  - fix a bug when sending message [0x48].
  - deprecate ADServer.setDamperPositions and put it in regular messages cycle.
  - update data structure. positionAutoOffset, manualPositionLevel, isAtAutoMode, and their related functions are added for Zone.

[![Generic badge](https://img.shields.io/badge/Release-v1.2-<COLOR>.svg)](https://shields.io/)
  - add read and write Intesis a/c status registers features

[![Generic badge](https://img.shields.io/badge/Release-v1.3-<COLOR>.svg)](https://shields.io/)
  - A "ADServer State changes when Duct/Relief is change" bug got fixed
  - Listner and ADServerCallback will not run on UI Thread

[![Generic badge](https://img.shields.io/badge/Release-v1.4-<COLOR>.svg)](https://shields.io/)
  - Remove ACModbus polling
  - Rename ADServer.readIntesisACStatus() to ADServer.readIntesisACRegister()
  - Remove "interpreted" Intesis methods, such as: ADServer.changeIndoorAmbientTemperature() and getFanSpeed()
  - ADServer.writeIntesisACRegister "registerValue" is an int not a list now.
  - ADServer.readIntesisACStatus() to ADServer.readIntesisACRegister() will return an Integer value of "registerValue" instead of an array of String.
  - fix huge influx of update events

This document details how to use the “ADController Modules API” to communicate with Air Diffusion Platinum II Damper control (RMC04) and other devices connected on the RS485 communications link.

### Features
  - Act as a Server and maintain data by itself.
  - The Server will send out flags whenever there is a status or data change.
  - Enable users to send different messages to the Air Diffusion Platinum II System.

### Basic Project Structure
This project contains two parts: one is the module called "adserverlibrary"; the other folder called "app" contains UI code for testing the "adserverlibrary" library.

### Generate AAR file
In order to generate the AAR file of the library "adserverlibrary", please see the below steps:
 - go to "Build" on the Android Studio menu
 - click "Make Selected Modules"
 - find the AAR file under directory ProjectName->adserverlibrary->build->outputs->arr

### Generate JavaDoc
Please see the following steps to generate the JavaDoc files
 - go to "Tools" on the Android Studio menu
 - click "Generate JavaDoc..."
 - Pick the "Generate JavaDoc scope", select your output directory and click "OK".

### Installation
There are two ways to install the API:<br><br>
One way is cloning the whole project from the project repository. Everything is already set up. There are examples inside this project showing how to use the "adserverlibrary".<br><br>
The other way is importing "adserverlibrary" module to your project. There are several steps:
1. Download this project.
2. Go to your project and import the "adserverlibrary" module.
3. Open the app module's build.gradle file and add a new line to the dependencies block as shown in the following snippet:
```gradle
implementation project(":adserverlibrary")
```
4. Add jitpack.io repository to your root build.gradle
```gradle
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```
Also add a library to dependencies if the module is imported by AAR file.
```gradle
implementation 'com.github.mik3y:usb-serial-for-android:Tag'
```
5. Add [usb_device_filter.xml](https://gitlab.com/platinum-tablet/control-service-test-application/-/blob/master/app/src/main/res/xml/usb_device_filter.xml) to your project's res/xml/ directory and configure in your AndroidManifest.xml as the following:
```xml
<activity
    android:name="..."
    ...>
    <intent-filter>
        <action android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED" />
    </intent-filter>
    <meta-data
        android:name="android.hardware.usb.action.USB_DEVICE_ATTACHED"
        android:resource="@xml/usb_device_filter" />
</activity>
```

### How To Use
Class ADServer is the class that acts as a server. It is also a singleton class.

First, Initialize ADServer in the Activity classes.
```java
ADServer.init(this);
```

After the initialization, the singleton class ADSever will create an static instance inside the class. The following shows that the way to get the instance.
```java
ADServer.getMInstance()
```

Start the server.
```java
ADServer.getMInstance().startUp();
```

Stop the server.
```java
ADServer.getMInstance().stop();
```

Monitor any change of the state of the server and the server's data. The update() function will be triggered anytime when there is a state and data change.
```java
ADServer.getMInstance().setupAdListener(new ADListener() {
    @Override
    public void update() {
        //codes
    }
});
```

Get the server state. There are 4 states. They are OFF, CHECKING, RUNNING, and INITIALIZING. If there is any state change, the ADListener will be triggered.

- OFF: means the server is waiting to start or there is an error.
- CHECKING: means the server is checking the Air Diffusion System, such as: how many damper controls? What is their serial number ID?
- RUNNING: means the server is in a normal state.
- INITIALIZING: means the damper controls are setting up. The server will wait for it to finish and will be at RUNNING when it is done.

Here is how the server works:
When the server starts up, the state will change to CHECKING from OFF. It will send out a message [0x5F] and a message [0x5C] to check the Air Diffusion System. If there is any error, the server will be back to OFF.
After the CHECKING is successfully done, the server will send out a message [0x16] to turn on all the damper controls. If there is any error, the state of the server will be OFF.
When it is successfully done, the server will start a process which will periodically send out a message [0x0B] to check and get data from the system. If the system tells the server that the damper controls is setting up, the server will be at INITIALIZING state. Otherwise, it will be in a RUNNING state. When it is at RUNNING state, it will periodically send out a message [0x07], a message [0x09], and a message [0x0C] to get the extra data from the system to populate its ADsystem.
During this periodical process, the server will be off and the state will be OFF if there is any error.
```java
ADServer.getMInstance().getADState();
```

Get the server error message. If there is any error occurred after the server starts up, an error message will be generated and the ADListener will be triggered.
```java
ADServer.getMInstance().getMErrMsg();
```

When the server is at RUNNING state, It will periodically send out a message [0x0B], a message [0x07], a message [0x09], and a message [0x0C] to get basic status from the system. The following function is used to set up how frequently the server gets basic status from the Air Diffusion System. The parameter of this function means how long the server will wait to get data from the system again. The default is 5 seconds. The parameter value needs to be more than 5.
```java
ADServer.getMInstance().setMTotalWaitingSeconds(10);
```

Get the server data object ADSystem. ADSystem has two object arrays. One is DamperControls, the other one is SystemInterfaces. For example, If there are two damper controls connecting to the system, DamperControls will show 2 items in the array. So does SystemInterfaces. But normally the Air Diffusion System only has one or none System Interface. Any data change inside ADSystem will trigger the ADListener.
```java
ADServer.getMInstance().getAdSystem();
```

##### ADSystem Data Structure
R: able to read from the hardware; W: able to write from the hardware; f: related function
- ADSystem &emsp;(f: ADServer.getAdSystem)
  - DamperControls &emsp;(f: getDamperControls)
    - serialNumberId &emsp;(R: msg[0x5F]; f: getSerialNumberId)
    - version &emsp;(R: msg[0x03]; f: getVersion)
    - isOn &emsp;(W: msg[0x16]; f: getIsOn, ADServer.turnACOnOff)
    - isMaster &emsp;(f: getIsMaster)
    - zones &emsp;(f: getZones)
      - isDamperLoaded &emsp;(R: msg[0x0B]; f: getIsDamperLoaded)
      - isWiredTempSensorLoaded &emsp;(R: msg[0x0B]; f: getIsWiredTempSensorLoaded)
      - isFaultForSensorValueWired &emsp;(R: msg[0x0B]; f: getIsFaultForSensorValueWired)
      - isRFTempSensorLoaded &emsp;(R: msg[0x0B]; f: getIsRFTempSensorLoaded)
      - isRFTempSensorLearnt &emsp;(R: msg[0x0B]; f: getIsRFTempSensorLearnt)
      - isRFTempSensorFault &emsp;(R: msg[0x0B]; f: getIsRFTempSensorFault)
      - isRFTempSensorLowBattery &emsp;(R: msg[0x0B]; f: getIsRFTempSensorLowBattery)
      - isInError &emsp;(R: msg[0x07] & [0x0B]; f: getIsInError)
      - isIncludedInCalculatingAvgTemp &emsp;(R: msg[0x0B]; f: getIsIncludedInCalculatingAvgTemp)
      - positionLevel &emsp;(R: msg[0x07]; f: getPositionLevel)
      - positionAutoOffset &emsp;(W: msg[0x48]; f: getPositionAutoOffset, setPositionAutoOffset, ADServer.setDamperPositions)
      - manualPositionLevel &emsp;(W: msg[0x48]; f: getManualPositionLevel, setManualPositionLevel, ADServer.setDamperPositions)
      - isAtAutoMode &emsp;(W: msg[0x48]; f: getIsAtAutoMode, setIsAtAutoMode, ADServer.setDamperPositions)
      - isSelectedRelief &emsp;(R: msg[0x07] & [0x0C]; W: msg[0x4C]; f: getIsSelectedRelief, ADServer.setZoneModelAndReliefZone)
      - temperature &emsp;(R: msg[0x09]; f: getTemperature)
      - zoneModel &emsp;(R: msg[0x0C]; W: msg[0x4C]; f: getZoneModel, ADServer.setZoneModelAndReliefZone)
      - isReliefActive &emsp;(R: msg[0x0C]; f: getIsReliefActive)
      - isAdvancedOptionReturn &emsp;(R: msg[0x60]; W: msg[0x61]; f: getIsAdvancedOptionReturn, ADServer.setZoneAdvancedOptionsReturn)
      - isAdvancedOptionOutside &emsp;(R: msg[0x60]; W: msg[0x61]; f: getIsAdvancedOptionOutside, ADServer.setZoneAdvancedOptionsOutside)
      - isAdvancedOptionLock &emsp;(R: msg[0x60]; W: msg[0x61]; f: getIsAdvancedOptionLock, ADServer.setZoneAdvancedOptionsLock)
      - isAdvancedOptionNotVisible &emsp;(R: msg[0x60]; W: msg[0x61]; f: getIsAdvancedOptionNotVisible, ADServer.setZoneAdvancedOptionsInvisible)
      - advancedVarMinBalance &emsp;(R: msg[0x62]; W: msg[0x63]; f: getAdvancedVarMinBalance, ADServer.setZoneAdvancedMinMaxBalance)
      - advancedVarMaxBalance &emsp;(R: msg[0x62]; W: msg[0x63]; f: getAdvancedVarMaxBalance, ADServer.setZoneAdvancedMinMaxBalance)
      - eePromAddressID &emsp;(R: msg[0x12]; W: msg[0x24] & [0x25]; f: getEePromAddressID, ADServer.clearRFWirelessSensor, ADServer.connectRFWirelessSensor)
    - ductTemperature &emsp;(R: msg[0x09]; f: getDuctTemperature)
    - autoAverageTemperature &emsp;(R: msg[0x09]; f: getAutoAverageTemperature)
    - ecoActivateTemperature &emsp;(R: msg[0x64]; W: msg[0x65]; f: getEcoActivateTemperature, ADServer.setEcoActivateTemp)
    - AdvanceSystemOptions &emsp;(f: getAdvanceSystemOptions)
      - isEcoEnable &emsp;(R: msg[0x0B] & [0x64]; W: msg[0x65]; f: getIsEcoEnable, ADServer.setAdvancedVarSysOptionsEcoEnable)
      - isEcoOn &emsp;(R: msg[0x0B] & [0x64]; f: getIsEcoOn)
      - isBalanceEnable &emsp;(R: msg[0x0B] & [0x64]; W: msg[0x65]; f: getIsBalanceEnable, ADServer.setAdvancedVarSysOptionsBalanceEnable)
      - isEcoOnOutputState &emsp;(R: msg[0x0B] & [0x64]; f: getIsEcoOnOutputState)
    - AutoModeAndDuctSensorFlags &emsp;(f: getAutoModeAndDuctSensorFlags)
      - isHeatingMode &emsp;(R: msg[0x0B]; f: getIsHeatingMode)
      - isModeAdetect &emsp;(R: msg[0x0B]; f: getIsModeAdetect)
      - isDuctSensorValid &emsp;(R: msg[0x0B]; f: getIsDuctSensorValid)
      - isDuctSensorFault &emsp;(R: msg[0x0B]; f: getIsDuctSensorFault)
      - isInStandby &emsp;(R: msg[0x0B]; f: getIsInStandby)
      - isSystemOff &emsp;(R: msg[0x0B]; f: getIsSystemOff)
  - SystemInterfaces(SIPS Module) &emsp;(f: getSystemInterfaces)
    - serialNumberId &emsp;(R: msg[0x5C]; f: getSerialNumberId)
    - isSIPSRelayOn &emsp;(R: msg[0x68]; f: getIsSIPSRelayOn)
    - zones &emsp;(f: getZones)
      - isMotionSensorInstalled &emsp;(R: msg[0x68]; f: getIsMotionSensorInstalled)
      - isPIRMotionDetected &emsp;(R: msg[0x68]; f: getIsPIRMotionDetected)
      - isPIRMotionInError &emsp;(R: msg[0x68]; f: getIsPIRMotionInError)
      - isTimerInstalled &emsp;(R: msg[0x68]; f: getIsTimerInstalled)
      - isTimerActivated &emsp;(R: msg[0x68]; f: getIsTimerActivated)
      - isTimerInError &emsp;(R: msg[0x68]; f: getIsTimerInError)
      - motionSensorTimeNumber &emsp;(R: msg[0x68]; W: msg[0x68]; f: getMotionSensorTimeNumber, ADServer.resetSystemInterfaceZoneMotionTimers)
  - ACModbus &emsp;(f: getACModbuses)
    - serialNumberId &emsp;(R: msg[0x5D]; f: getSerialNumberId)
    - version &emsp;(R: msg[0x74]; f: getVersion)
    - isOn &emsp;(R: msg[0x70]; W: msg[0x72]; f: getIsOn, ADServer.turnACOnOff)
    - mode &emsp;(R: msg[0x70]; W: msg[0x72]; f: getMode, ADServer.changeACMode)
    - fanSpeed &emsp;(R: msg[0x70]; W: msg[0x72]; f: getFanSpeed, ADServer.changeACFanSpeed)
    - temperatureSetpoint &emsp;(R: msg[0x70]; W: msg[0x72]; f: getTemperatureSetpoint, ADServer.changeACTemperatureSetpoint)
    - temperatureReference &emsp;(R: msg[0x70]; f: getTemperatureReference)
    - indoorAmbientTemperature &emsp;(R: msg[0x70]; W: msg[0x72]; f: getIndoorAmbientTemperature, ADServer.changeIndoorAmbientTemperature)
    - realTemperatureSetpoint &emsp;(R: msg[0x70]; f: getRealTemperatureSetpoint)
    - returnPathTemperature &emsp;(R: msg[0x70]; f: getReturnPathTemperature)
  - slaveAddress &emsp;(f: getSlaveAddress, setSlaveAddress)

Get the array damperControls.
```java
ADServer.getMInstance().getAdSystem().getDamperControls();
```
Get a specific damper control. The Air Diffusion System has up to 2 damper controls. If there are two damper controls, one will be a master.
```java
ArrayList<DamperControl> dcs = ADServer.getMInstance().getAdSystem().getDamperControls();
if (!dcs.isEmpty()) {
    DamperControl dc = dcs.get(0);
}
```

Or get a damper control by its serial number ID:
```java
DamperControl dc = ADServer.getMInstance().getAdSystem().getDamperControlBy(7001);
```

Get zones(array) from a damper control.
```java
dc.getZones();
```
Get a particular zone from a damper control.
getZones() will return an array with 8 items. Each item represents a zone of the damper control. The first item represents zone 1, the second item represents zone 2...
```java
if (!dc.getZones().isEmpty()) {
    Zone z1 = dc.getZones().get(0);
}
```

The following are examples how to get data out from a damper control:
Get a damper control's serial number id.
```java
dc.getSerialNumberId();
```
Get isDamperLoaded from a zone.
```java
z1.getIsDamperLoaded();
```
Get isHeatingMode from an AutoModeAndDuctSensorFlags object.
```java
dc.getAutoModeAndDuctSensorFlags().getIsHeatingMode();
```

Get the array SystemInterfaces.
```java
ADServer.getMInstance().getAdSystem().getSystemInterfaces();
```
Get a specific system interface. The Air Diffusion System only has one or none System Interface. If there are one system interface and two damper controls connected, the zones information inside the system interface only represents the master damper control's zones.
```java
ArrayList<SystemInterface> sis = ADServer.getMInstance().getAdSystem().getSystemInterfaces();
if (!sis.isEmpty()) {
    SystemInterface si = sis.get(0);
}
```

##### Sending out specific messages
There is some system's information that does not need to do a periodic query by the server, such as the damper control's version. To get these not so often needed information, it can be done by sending out specific messages whenever needed.
There are also other messages which are able to change the system state, such as changing a damper's position.
Note: The server can only send out one message to the system at a time. The server will wait to send a specific message if the periodical messages are in process sending to the system. A specific message will be sent out until that process is done.

The following are the examples for these specific messages:
###### Damper Control
'Get firmware version' from a damper control.<br>
First parameter is a damper control which the function will get version information from.<br>
Second parameter is ADServerCallback, a callback. When the query is done, the callback will be triggered.<br>
Uses a message [0x03] to a connected RMC04 to read its firmware version.
```java
ADServer.getMInstance().getDamperControlVersion(dcs.get(0), new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        if (!adSRes.isSuccessful) {
            //code...
        } else {
            //code...
        }
     }
});
```

Change damper position level from a damper control. This function will send out one message [0x48]. This message will set all the position level settings of all zones from a damper control, both auto mode and manual mode. It will also set the mode of each zone in this damper control (note: setting auto mode will work only if the zone has a temperature sensor). At last it will set the damper control in heating mode or cooling mode.
- The first parameter of this function is a damper control.
- The second parameter is a Boolean. It is used to set the damper control mode: heating or cooling.
note: It is used only when the system is first set up. It will be ignored once mode detected by the damper control.
- The last parameter is a callback.
```java
Zone zone = dc.getZones().get(zoneIndex);
zone.setManualPositionLevel(7);
//ADServer.getMInstance().setDamperPositions(dc, false, new ADServerCallback() {
//    @Override
//    public void result(ADServerResponse adSRes) {
//        //code...
//    }
//});
Note: ADServer.getMInstance().setDamperPositions is deprecated for now. The system will automatically send out the message [0x48] every cycle.
```

'Reset Defaults' from any connected damper controls (RMC04). Only requires one parameter callback.<br>
Uses a message [0x49] to clear any previously installed components saved in EEprom memory.<br>
Note that all previously installed components such as zone dampers, associated wired or wireless temperature sensors and their IDs' selected 'relief zones etc. will be cleared.
```java
ADServer.getMInstance().resetDamperControls(new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        if (!adSRes.isSuccessful) {
            //code...
        } else {
            //code...
        }
    }
});
```

'Install' to any connected damper controls (RMC04). Only requires one parameter callback.<br>
Uses a message [0x4E] to start the RMC04(s) to install of attached devices.<br>
The RMC04 will sequence through all attached zone dampers and any attached wired temperature sensors. When complete these settings will be stored in EEprom, so if power is removed from the RMC04, settings can be restored on power being restored.
```java
ADServer.getMInstance().installDamperControls(new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        if (!adSRes.isSuccessful) {
            //code...
        } else {
            //code...
        }
    }
});
```

Get Wireless Sensor ID(s) from EE-prom on the RMC04 (at assigned address for each zone).
Uses the message type [0x12] to read the IDs stored in EE-prom for selected zone(s) (up to 8 in one RMC04).<br>
The following example shows how to get all installed Wireless sensor IDs for one damper control (up to 8 IDs).
Note: Only a zone which is loaded and its RF wireless sensor is loaded and learnt will have a valid ID in EE-prom.
```java
ADServer.getMInstance().getEEpromAddressIDs(dc, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
The following, allows for reading a single Wireless Sensor ID from the EE-prom for a selected zone.
```java
ADServer.getMInstance().getEEpromAddressID(dc, zoneIndex, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
Clear a zone Wireless Sensor ID from EEprom.<br>
Uses a message [0x24] to clear a selected ID from the EE prom in the RMC04.
```java
ADServer.getMInstance().clearRFWirelessSensor(dc, zoneIndex, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
Connect (learn) a Wireless sensor ID and store in RMC04 EE-prom for a selected zone.<br>
Uses message type [0x25] to request the main RMC04 to search for a Wireless Sensor ID and store in an EE-prom address, associated with the selected zone.<br>
After sending out this request, the Server will send a message [0x0B] every second for up to 10 seconds, and monitor the status of the relevant zone.<br>
If successful, the Wireless sensor ID has been learnt and stored in the EEprom, The Server will send a message [0x12] to read ID for relevant zone in EEprom address from the RMC04.<br>
If after 10 seconds, the Wireless sensor ID was not detected, the RMC04 stops searching (failed to learn).
The user will have to try again.

```java
ADServer.getMInstance().connectRFWirelessSensor(dc, zoneIndex, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
###### AC Modbus
Get AC Modbus version.<br>
Uses a message [0x74] to a connected modbus to read its firmware version.
```java
mAdController.getModbusVersion(acModbus, new ADCallback() {
    @Override
    public void result(Response res) {
        //code...
    }
});
```
Request to read Intesis a/c status registers.<br>
Uses a message [0x70] request to read Intesis a/c status registers.<br>
ADServerResponse.registerValue (Integer) shows the response value.
```java
ADServer.getMInstance().readIntesisACRegister(acModbus, modbusAddressInt, registerAddressInt, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
   }
});
```
Request to write Intesis a/c status registers.<br>
Uses a message [0x72] request to write Intesis a/c status registers.<br>
ADServerResponse.registerValue (Integer) shows the response value.
```java
ADServer.getMInstance().writeIntesisACRegister(acModbus, modbusAddressInt, registerAddressInt, registerValue, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```

###### Advanced Features
Read advanced_var zone_options settings from Damper Control.<br>
Uses a message [0x60] to read the following zone settings: <br>
- isAdvancedOptionReturn
- isAdvancedOptionOutside
- isAdvancedOptionLock
- isAdvancedOptionNotVisible
- isAdvancedOptionEnabledMotion
- isAdvancedOptionDetectedMotion
```java
ADServer.getMInstance().getAdvancedVarZoneOptions(dc, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
Write advanced_var zone_options settings to Damper Control.<br>
Uses a message [0x61] to set the advanced_var zone_options settings. <br>
The following is an example to set "isAdvancedOptionReturn":
```java
ADServer.getMInstance().setZoneAdvancedOptionsReturn(dc, zoneIndex, true, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
   }
});
```
Read advanced_var minmax_zone_balance settings from Damper Control.<br>
Uses a message [0x62] to read the following zone settings: <br>
- advancedVarMinBalance
- advancedVarMaxBalance
```java
ADServer.getMInstance().getAdvancedVarZoneMinMax(dc, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
Write advanced_var minmax_zone_balance settings to Damper Control.<br>
Uses a message [0x63] to set the advanced_var minmax_zone_balance settings.<br>
```java
ADServer.getMInstance().setZoneAdvancedMinMaxBalance(dc, zoneIndex, min, max, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
Read advanced_var advanced_sys_options & eco_activate_temp settings from Damper Control.<br>
Uses a message [0x64] to read the following AdvanceSystemOptions settings: <br>
- isEcoEnable
- isEcoOn
- isBalanceEnable
- isEcoOnOutputState
and ecoActivateTemperature.
```java
ADServer.getMInstance().getAdvancedVarSysOptionsAndEcoActivateTemp(dc, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
Write advanced_var advanced_sys_options & eco_activate_temp settings to Damper Control.<br>
Uses a message [0x65] to set the settings.<br>
The following are two examples to set "isEcoEnable" and "ecoActivateTemperature":
```java
ADServer.getMInstance().setAdvancedVarSysOptionsEcoEnable(dc, true, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```
```java
ADServer.getMInstance().setEcoActivateTemp(dc, ecoActTemp, new ADServerCallback() {
    @Override
    public void result(ADServerResponse adSRes) {
        //code...
    }
});
```

### Error Messages
 - "...: No Response." means there is no response from the hardware system within 0.5 seconds; need to check the connection or the hardware.
 - "...: Set Up Failed." means the Serial USB setup is failed; need to check the connection.
 - "Need the damper ID." means the function's parameter "DamperControl" needs to be valid.
 - "Need the System Interface ID." means the function's parameter "SystemInterface" needs to be valid.
 - "Need the AC Modbus ID." means the function's parameter "ACModbus" needs to be valid.
 - "No Damper Controls, System Interfaces, and AC Modbuses found!" means there are no damper controls, SIPS modules, and AC modbuses connected to the application.
 - "Fail to identify master Damper Control!" means The API fails to identify the master damper control when there are two damper controls connected to the system.
 - "Please wait for the previous message to be sent." means the API asks the system to send the message later because the API is limiting the frequency of sending messages out. (0.5 seconds per message)
 - "The Server needs to be in a RUNNING state." means that the API restricts some messages only can be sent out when the API is at RUNNING state.
 - "zoneIndex is out of range. (must be between 0 and 7)" means the function needs to have a valid zoneIndex input.
 - "Fail to connect a RF wireless sensor." means the API fails to connect a RF wireless sensor when the function connectRFWirelessSensor is called.
 - "The Max value needs to be greater than Min Value." means the function setZoneAdvancedMinMaxBalance needs the value of parameter Max is greater than the value of parameter Min.
 - "Please set it on the master damper control." means the parameter "DamperControl" of the function setAdvancedVarSysOptionsEcoEnable and setAdvancedVarSysOptionsBalanceEnable is required to be a master damper control when there are two damper control are connected.
 - "The slaveAddress needs to be inside of {0, 63}" means the parameter "value" of setSlaveAddress is required to be between 0 and 63.